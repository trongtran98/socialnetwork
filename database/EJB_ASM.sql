CREATE DATABASE ASMEJB02
go
USE ASMEJB02
GO
CREATE TABLE role
(
	roleid VARCHAR(50) PRIMARY KEY,
	rolename VARCHAR(250)
)
GO
CREATE TABLE account
(
	username VARCHAR(250) PRIMARY KEY,
	password VARCHAR(250),
	roleid VARCHAR(50) FOREIGN KEY REFERENCES dbo.role(roleid),
	avatar VARCHAR(250),
	firstname NVARCHAR(250),
	lastname NVARCHAR(250),
	email VARCHAR(250),
	nationality nvarchar(250),
	relition nvarchar(250)
)
GO
CREATE TABLE guide 
(
	guideid VARCHAR(50) PRIMARY KEY,
	title NVARCHAR(250),
	place NVARCHAR(250),
	description NTEXT,
	image VARCHAR(250),
	username VARCHAR(250) FOREIGN KEY REFERENCES dbo.account(username)
)
GO
CREATE TABLE comment
(
	commentid varchar(50) PRIMARY KEY,
	content NVARCHAR(250),
	guideid VARCHAR(50) FOREIGN KEY REFERENCES dbo.guide(guideid),
	username VARCHAR(250) FOREIGN KEY REFERENCES dbo.account(username)
)
GO
CREATE TABLE _like
(
	likeid VARCHAR(250) PRIMARY KEY,
	guideid VARCHAR(50) FOREIGN KEY REFERENCES dbo.guide(guideid),
	username VARCHAR(250) FOREIGN KEY REFERENCES dbo.account(username)
)

INSERT dbo.role
        ( roleid, rolename )
VALUES  ( 'role1', -- roleid - varchar(50)
          'user'  -- rolename - varchar(250)
          )

INSERT dbo.account
        ( username ,
          password ,
          roleid ,
          avatar ,
          firstname ,
          lastname ,
          email ,
          nationality ,
          relition
        )
VALUES  ( 'admin' , -- username - varchar(250)
          'admin' , -- password - varchar(250)
          'role1' , -- roleid - varchar(50)
          'admin.jpg' , -- avatar - varchar(255)
          N'Admin' , -- firstname - nvarchar(250)
          N'Admin' , -- lastname - nvarchar(250)
          'admin@gmail.com' , -- email - varchar(250)
          N'VN' , -- nationality - nvarchar(250)
          N'No'  -- relition - nvarchar(250)
        ),
		(
			'trongtran' , -- username - varchar(250)
			'trongtran' , -- password - varchar(250)
			'role1' , -- roleid - varchar(50)
			'trongtran.jpg' , -- avatar - varchar(255)
			N'Trọng' , -- firstname - nvarchar(250)
			N'Trần' , -- lastname - nvarchar(250)
			'trongtran@gmail.com' , -- email - varchar(250)
			N'VN' , -- nationality - nvarchar(250)
			N'No'  -- relition - nvarchar(250)
		)

INSERT dbo.guide
        ( guideid ,
          title ,
          place ,
          description ,
          image ,
          username
        )
VALUES  ( 'guide1232131231' , -- guideid - varchar(50)
          N'' , -- title - nvarchar(250)
          N'Hà Nội, Việt Nam' , -- place - nvarchar(250)
          N'Hà Nội hiện nay có hơn 4.000 đài tưởng niệm và cảnh quan đẹp. Trong đó, có hơn 9000 di tích được nằm trong bảng xếp hạng các di tích quốc gia, với hàng trăm ngôi chùa, công trình kiến trúc và danh lam thắng cảnh nổi tiếng. So với các tỉnh, thành phố khác ở Việt Nam, Hà Nội có tiềm năng phát triển du lịch rất lớn. Ngay trong các quận nội thành, cùng với các công trình kiến trúc, Hà Nội đang sở hữu một hệ thống bảo tàng quy mô lớn. Một số di tích nổi bật khác của Hà Nội đó là Văn Miếu Quốc Tử Giám (trường đại học đầu tiên của Việt Nam), Chùa Một Cột (tượng trưng cho cái nôi của Phật giáo và Đạo giáo), Đền Ngọc Sơn, Quảng trường Ba Đình (nơi bác Hồ đọc bản tuyên ngôn độc lập), Nhà thờ Chính tòa Hà Nội, Nhà Hát lớn, v.v.' , -- description - ntext
          'hanoi.jpg' , -- image - varchar(250)
          'trongtran'  -- username - varchar(250)
        )