package com.trongtran.repositories.RoleRepo;

import com.trongtran.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends PagingAndSortingRepository<Role,String>,RoleRepoCustom {
}
