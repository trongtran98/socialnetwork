package com.trongtran.repositories.LikeRepo;

import com.trongtran.entity.Account;
import com.trongtran.entity.Guide;
import com.trongtran.entity.Like;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeRepo extends PagingAndSortingRepository<Like,Integer>, LikeRepoCustom{
    @Query("from Like l where l.username = :username and l.guideid = :guideid")
    public Like checkAccountLikeByGuideIdAndUsername(@Param("username") Account username,@Param("guideid") Guide guideid );
}
