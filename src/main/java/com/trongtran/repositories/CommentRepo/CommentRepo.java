package com.trongtran.repositories.CommentRepo;

import com.trongtran.entity.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepo extends PagingAndSortingRepository<Comment,String>,CommentRepoCustom {
}
