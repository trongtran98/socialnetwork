package com.trongtran.repositories.AccountRepo;

import com.trongtran.entity.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepo extends PagingAndSortingRepository<Account,String>,AccountRepoCustom {
    @Query(value = "from Account a where a.username = :username and a.password = :password")
    Account doLogin(@Param("username") String username,@Param("password") String password);

}
