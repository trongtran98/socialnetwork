package com.trongtran.repositories.GuideRepo;

import com.trongtran.entity.Guide;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GuideRepo extends PagingAndSortingRepository<Guide,String>, GuideRepoCustom{
    @Query("from Guide g where g.place like CONCAT('%',:place,'%')")
   List<Guide> searchByPlace(@Param("place") String place);
}
