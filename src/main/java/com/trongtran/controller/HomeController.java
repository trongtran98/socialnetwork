package com.trongtran.controller;


import com.trongtran.entity.Account;
import com.trongtran.entity.Comment;
import com.trongtran.entity.Guide;
import com.trongtran.entity.Like;
import com.trongtran.repositories.LikeRepo.LikeRepo;
import com.trongtran.services.AccountService.AccountService;
import com.trongtran.services.CommentService.CommentService;
import com.trongtran.services.GuideService.GuideService;
import com.trongtran.services.LikeService.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    GuideService guideService;
    @Autowired
    AccountService accountService;
    @Autowired
    CommentService commentService;
    @Autowired
    LikeService likeService;

    @RequestMapping(value = {"","{page}"})
    public String home(ModelMap modelMap, HttpServletRequest httpServletRequest,@PathVariable Optional<Integer> page) {
        HttpSession session = httpServletRequest.getSession();
        String username = (String) session.getAttribute("username");
        if (username != null) {

            modelMap.addAttribute("user",accountService.findById(username));
            modelMap.addAttribute("pages",guideService.totalPage());
            modelMap.addAttribute("listGuide", guideService.listAll((page.orElse(null)==null)?0:page.get()));
            return "index";
        }
        return "login";
    }

    @RequestMapping(value = "addguide",method = RequestMethod.POST)
    public String addGuide(Guide guide, @RequestParam("guideFile") MultipartFile guideFile, HttpServletRequest request) {
        Account account = accountService.findById((String) request.getSession().getAttribute("username"));
        guide.setGuideid("guideid" + System.currentTimeMillis());
        String fileName = "guideimg" + System.currentTimeMillis() + ".jpg";
        guide.setImage(fileName);
        guide.setUsername(account);
        if (upFile(guideFile, request, fileName)) {
            guideService.updateGuide(guide);
            System.out.println("add guide ok");
            return "redirect:/";
        }
        System.out.println("add guide fail");
        return "redirect:/";
    }

    @RequestMapping(value = "addcomment",method = RequestMethod.POST)
    public String addComment(Comment comment, @RequestParam("id") String guideId,HttpServletRequest request) {
        comment.setCommentid("comment"+System.currentTimeMillis());
        comment.setGuideid(guideService.findById(guideId));
        Account account = accountService.findById((String) request.getSession().getAttribute("username"));
        comment.setUsername(account);
        if(commentService.addComment(comment)){
            System.out.println("add comment successfully");
            return "redirect:/";
        }
        System.out.println("add comment fail");
        return "redirect:/";
    }

    @RequestMapping("like/{guideid}")
    public String like(ModelMap modelMap, HttpServletRequest httpServletRequest, @PathVariable("guideid") String guideid) {
        HttpSession session = httpServletRequest.getSession();
        String username = (String) session.getAttribute("username");
        Like liketemp =likeService.checkAccountLikeByGuideIdAndUsername(accountService.findById(username),guideService.findById(guideid));
        if(liketemp==null){
            Like like = new Like();
            like.setLikeid("like"+System.currentTimeMillis());
            like.setGuideid(guideService.findById(guideid));
            like.setUsername(accountService.findById(username));
            likeService.like(like);
            return "redirect:/";
        }else {
            likeService.dislike(liketemp);
            return "redirect:/";
        }
    }

    public boolean upFile(MultipartFile file, HttpServletRequest request, String fileName) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
//                String path = request.getSession().getServletContext().getRealPath("/WEB-INF/images/");
                // Creating the directory to store file
                String path = ResourceUtils.getFile("classpath:images/guide").getPath();
                BufferedOutputStream bout = new BufferedOutputStream(
                        new FileOutputStream(path + "/"+fileName));
                bout.write(bytes);
                bout.flush();
                bout.close();
                System.out.println("write file successfully!");
                return true;
            } catch (Exception e) {
                System.out.println("write file failure!");
                e.printStackTrace();
                return false;
            }
        } else {
            System.out.println("File is empty!");
            return false;
        }
    }
}
