package com.trongtran.controller;

import com.trongtran.services.AccountService.AccountService;
import com.trongtran.services.GuideService.GuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/search")
public class SearchController {
    @Autowired
    GuideService guideService;
    @Autowired
    AccountService accountService;

    @RequestMapping(value = "")
    public String search(@RequestParam("keyword") String keyword, ModelMap modelMap, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username != null) {
            modelMap.addAttribute("user", accountService.findById(username));
            modelMap.addAttribute("listGuide", guideService.searchByPlace(keyword));
            return "index";
        }
        return "login";
    }
}