package com.trongtran.controller;

import com.trongtran.entity.Account;
import com.trongtran.services.AccountService.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

@Controller
@RequestMapping("/account")
public class AccountController {
    @Autowired
    AccountService accountService;

    @RequestMapping(value = "/dologin", method = RequestMethod.POST)
    public String doLogin(HttpServletRequest httpServletRequest, Account account) {
        if (accountService.doLogin(account)) {
            HttpSession session = httpServletRequest.getSession();
            session.setAttribute("username", account.getUsername());
            return "redirect:/";
        }
        return "login";
    }

    @RequestMapping(value = "/dologout")
    public String doLogout(HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().invalidate();
        return "login";
    }

    @RequestMapping(value = "/profile/{username}")
    public String profile(@PathVariable("username") String username, ModelMap modelMap) {
        modelMap.addAttribute("user", accountService.findById(username));
        return "profile";
    }

    @RequestMapping(value = "/myprofile")
    public String myProfile(ModelMap modelMap, HttpServletRequest request) {
        String myUsername = (String) request.getSession().getAttribute("username");
        modelMap.addAttribute("user", accountService.findById(myUsername));
        return "myprofile";
    }

    @RequestMapping(value = "/updateprofile", method = RequestMethod.POST)
    public String updateProfile(Account account, HttpServletRequest request, @RequestParam("avatarFile") MultipartFile avatarFile) {
        String myUsername = (String) request.getSession().getAttribute("username");
        Account root = accountService.findById(myUsername);
        String fileName = "avatar" + System.currentTimeMillis() + ".jpg";
        root.setAvatar(fileName);
        root.setFirstname(account.getFirstname());
        root.setLastname(account.getLastname());
        root.setEmail(account.getEmail());
        root.setNationality(account.getNationality());
        root.setRelition(account.getRelition());
        if (!avatarFile.isEmpty()) {
            upFile(avatarFile, fileName);
        }

        if (accountService.updateProfile(root)) {
            return "redirect:/account/myprofile";
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/createaccount")
    public String createAccount(Account account,HttpServletRequest request){
        if(accountService.updateProfile(account)){
            request.getSession().setAttribute("username",account.getUsername());
            return "redirect:/";
        }
        return "login";
    }

    public boolean upFile(MultipartFile file, String fileName) {

        try {
            byte[] bytes = file.getBytes();
//                String path = request.getSession().getServletContext().getRealPath("/WEB-INF/images/");
            // Creating the directory to store file
            String path = ResourceUtils.getFile("classpath:images/avatar").getPath();
            BufferedOutputStream bout = new BufferedOutputStream(
                    new FileOutputStream(path + "/" + fileName));
            bout.write(bytes);
            bout.flush();
            bout.close();
            System.out.println("write file successfully!");
            return true;
        } catch (Exception e) {
            System.out.println("write file failure!");
            e.printStackTrace();
            return false;
        }

    }
}
