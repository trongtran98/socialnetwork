package com.trongtran.entity;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class Comment {
    @Id
    private String commentid;
    private String content;
    @JoinColumn(name = "guideid", referencedColumnName = "guideid")
    @ManyToOne(fetch = FetchType.EAGER)
    private Guide guideid;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne(fetch = FetchType.EAGER)
    private Account username;

    public Comment() {
    }

    public Account getUsername() {
        return username;
    }

    public void setUsername(Account username) {
        this.username = username;
    }

    public String getCommentid() {
        return commentid;
    }

    public void setCommentid(String commentid) {
        this.commentid = commentid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Guide getGuideid() {
        return guideid;
    }

    public void setGuideid(Guide guideid) {
        this.guideid = guideid;
    }
}
