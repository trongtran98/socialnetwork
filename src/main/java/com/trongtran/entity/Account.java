package com.trongtran.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "account")
public class Account {
    @Id
    private String username;
    private String password;
    private String avatar;
    private String firstname;
    private String lastname;
    private String email;
    private String nationality;
    private String relition;
    @JoinColumn(name = "roleid", referencedColumnName = "roleid")
    @ManyToOne(fetch = FetchType.EAGER)
    private Role roleid;
    @OneToMany(mappedBy = "username", fetch = FetchType.EAGER)
    private List<Guide> guideList;
    @OneToMany(mappedBy = "username", fetch = FetchType.EAGER)
    private List<Comment> commentList;
    @OneToMany(mappedBy = "username", fetch = FetchType.EAGER)
    private List<Like> likeList;

    public Account(){}

    public List<Like> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<Like> likeList) {
        this.likeList = likeList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getRelition() {
        return relition;
    }

    public void setRelition(String relition) {
        this.relition = relition;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRoleid() {
        return roleid;
    }

    public void setRoleid(Role roleid) {
        this.roleid = roleid;
    }

    public List<Guide> getGuideList() {
        return guideList;
    }

    public void setGuideList(List<Guide> guideList) {
        this.guideList = guideList;
    }
}
