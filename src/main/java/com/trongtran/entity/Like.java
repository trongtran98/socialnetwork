package com.trongtran.entity;

import javax.persistence.*;

@Entity
@Table(name = "_like")
public class Like {
    @Id
    private String likeid;
    @JoinColumn(name = "guideid", referencedColumnName = "guideid")
    @ManyToOne(fetch = FetchType.EAGER)
    private Guide guideid;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne(fetch = FetchType.EAGER)
    private Account username;

    public Like(){

    }


    public String getLikeid() {
        return likeid;
    }

    public void setLikeid(String likeid) {
        this.likeid = likeid;
    }

    public Guide getGuideid() {
        return guideid;
    }

    public void setGuideid(Guide guideid) {
        this.guideid = guideid;
    }

    public Account getUsername() {
        return username;
    }

    public void setUsername(Account username) {
        this.username = username;
    }
}
