package com.trongtran.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "guide")
public class Guide {
    @Id
    private String guideid;
    private String title;
    private String place;
    private String description;
    private String image;
    @OneToMany(mappedBy = "guideid", fetch = FetchType.EAGER)
    private List<Comment> commentList;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne(fetch = FetchType.EAGER)
    private Account username;
    @OneToMany(mappedBy = "guideid", fetch = FetchType.EAGER)
    private Set<Like> likeList;

    public Guide() {
    }

    public Set<Like> getLikeList() {
        return likeList;
    }

    public void setLikeList(Set<Like> likeList) {
        this.likeList = likeList;
    }

    public String getGuideid() {
        return guideid;
    }

    public void setGuideid(String guideid) {
        this.guideid = guideid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public Account getUsername() {
        return username;
    }

    public void setUsername(Account username) {
        this.username = username;
    }
}
