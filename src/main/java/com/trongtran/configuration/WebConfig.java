package com.trongtran.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        super.addResourceHandlers(registry);
        registry.addResourceHandler(
                "webjars/**",
                "/images/**",
                "/avatars/**",
                "/css/**",
                "/js/**",
                "/static/**").addResourceLocations(
                        "classpath:/META-INF/resources/webjars/**",
                "classpath:/images/guide/",
                "classpath:/images/avatar/",
                "classpath:/static/css/",
                "classpath:/static/js/",
                "classpath:/static/");
    }
}
