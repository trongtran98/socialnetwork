package com.trongtran.services.LikeService;

import com.trongtran.entity.Account;
import com.trongtran.entity.Guide;
import com.trongtran.entity.Like;
import org.springframework.stereotype.Service;

@Service
public interface LikeService {
    boolean like(Like like);
    boolean dislike(Like like);
    Like checkAccountLikeByGuideIdAndUsername(Account username, Guide guideid);
}
