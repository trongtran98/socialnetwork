package com.trongtran.services.LikeService;

import com.trongtran.entity.Account;
import com.trongtran.entity.Guide;
import com.trongtran.entity.Like;
import com.trongtran.repositories.LikeRepo.LikeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LikeServiceImpl implements LikeService{
    @Autowired
    LikeRepo likeRepo;

    public boolean like(Like like) {
        try {
            likeRepo.save(like);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public boolean dislike(Like like) {
        try {
            likeRepo.delete(like);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public Like checkAccountLikeByGuideIdAndUsername(Account username, Guide guideid) {
        return likeRepo.checkAccountLikeByGuideIdAndUsername(username,guideid);
    }
}
