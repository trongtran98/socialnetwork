package com.trongtran.services.AccountService;

import com.trongtran.entity.Account;
import org.springframework.stereotype.Service;

@Service
public interface AccountService {
    boolean doLogin(Account account);
    Account findById(String username);
    boolean updateProfile(Account account);
}
