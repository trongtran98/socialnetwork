package com.trongtran.services.AccountService;

import com.trongtran.entity.Account;
import com.trongtran.repositories.AccountRepo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepo accountRepo;

    public boolean doLogin(Account account) {
        if (accountRepo.doLogin(account.getUsername(), account.getPassword()) != null)
            return true;
        return false;
    }

    public Account findById(String username) {
        return accountRepo.findOne(username);
    }

    public boolean updateProfile(Account account) {
        try {
            accountRepo.save(account);
            return true;
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
}
