package com.trongtran.services.GuideService;

import com.trongtran.entity.Guide;
import com.trongtran.repositories.GuideRepo.GuideRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuideServiceImpl implements GuideService {
    @Autowired
    GuideRepo guideRepo;

    public boolean updateGuide(Guide guide) {
        try {
            guideRepo.save(guide);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public Page<Guide> listAll(Integer page) {
        return guideRepo.findAll(new PageRequest(page,1));
    }

    public int totalPage() {
        return guideRepo.findAll(new PageRequest(0,1)).getTotalPages();
    }

    public Guide findById(String guideId) {
        return guideRepo.findOne(guideId);
    }

    public List<Guide> searchByPlace(String place) {
        return guideRepo.searchByPlace(place);
    }
}
