package com.trongtran.services.GuideService;

import com.trongtran.entity.Guide;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GuideService {
    boolean updateGuide(Guide guide);
    Page<Guide> listAll(Integer page);
    Guide findById(String guideId);
    List<Guide> searchByPlace(String place);
    int totalPage();
}
