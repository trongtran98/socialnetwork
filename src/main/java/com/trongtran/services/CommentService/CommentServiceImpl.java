package com.trongtran.services.CommentService;

import com.trongtran.entity.Comment;
import com.trongtran.repositories.CommentRepo.CommentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepo commentRepo;
    public boolean addComment(Comment comment) {
        try {
            commentRepo.save(comment);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
