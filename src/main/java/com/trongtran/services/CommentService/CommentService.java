package com.trongtran.services.CommentService;

import com.trongtran.entity.Comment;
import org.springframework.stereotype.Service;

@Service
public interface CommentService {
    boolean addComment(Comment comment);
}
